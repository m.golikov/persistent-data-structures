﻿namespace PersistentDataStructures.Persistency
{
    public interface IUndoRedo<out T>
    {
        public T Undo();
        public T Redo();
    }
}